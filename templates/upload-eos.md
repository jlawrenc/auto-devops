# EOS Sync template

## Deploying to an EOS backed YUM repository

```yaml
RPMs:
  stage: stage name
  extends: .auto_devops_upload_yum_repo_eos_template
  variables:
    # folder containing *.rpm files
    LOCAL_FOLDER: build/rpm
    # remote eos folder to sync to
    CERNBOX_FOLDER: /eos/user/u/username/www/username/yumrepos/myproject/$CI_COMMIT_REF_NAME/centos7_x86_64/RPMS/
```

This template will update the yum repository database files as well.

## Deploying generic files to EOS

```yaml
coverage:
  stage: publish
  extends: .auto_devops_upload_eos_template
  variables:
    LOCAL_FOLDER: build/coverage
    CERNBOX_FOLDER: /eos/user/u/username/www/username/
```

This template will copy all files in `$LOCAL_FOLDER` to `$CERNBOX_FOLDER`.

## A note on security and access rights

For these templates to work, you need to set `$AUTO_DEVOPS_CERNBOX_USER` and `$AUTO_DEVOPS_CERNBOX_PASS`.

You will notice that these are not present in the `variables: ` section of the yaml config.
These are set using [project CI variables](https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui).

Please do not use your personal account credentials.

It is highly recommended to set up a [service account](https://account.cern.ch/account/Help/?kbid=011010)

### EOS access rights

Assigning read/write rights to your service account works much the same like described in the [eos website setup docs](https://cernbox-manual.web.cern.ch/cernbox-manual/en/web/personal_website_content.html#create-personal-website-via-web-services).

Go to [cernbox](https://cernbox.cern.ch), and traverse to the folder that you wish to sync with. Assign your user read+write access by adding `a:username` to the share settings. Remember to tick the `write` box.

The account you use for EOS syncing does not need to be able to SSH into any host. The template uses the EOS API directly.

IMPORTANT: when creating folders, it only inherits sharing rights from its direct descendants.  
For example, when one wishes to sync to `/eos/user/u/username/www/username/yumrepos/myproject/$CI_COMMIT_REF_NAME/centos7_x86_64/RPMS/`, you need to set the share settings on the `/eos/user/u/username/www/username/yumrepos/myproject/` folder at the moment it has no subfolders yet.  
If say `/eos/user/u/username/www/username/yumrepos/myproject/master` already existed, the template would first successfully create the `/eos/user/u/username/www/username/yumrepos/myproject/master/centos7_x86_64/RPMS/` folder, but any copy commands would fail because it has no write permissions. The permissions did not exist on the `master` folder and thus did not propagate on subfolder creation.
