# docker-builder template

## autotag
Example use with auto-tagging:
```yaml
jobname:
  stage: stage name
  extends: .auto_devops_docker_builder_autotag
  variables:
    # docker context folder, defaults to '.'
    CONTEXT_FOLDER: .gitlab/ci
    # path to dockerfile, it is assumed to be within $CONTEXT_FOLDER
    # defaults to 'Dockerfile'
    DOCKERFILE: builder.Dockerfile
    # [optional] name of the docker image (without registry.gitlab.cern.ch/namespace/projectname prefix)
    # if omitted, the resulting image will be registry.gitlab.cern.ch/namespace/projectname
    NAME: builder
    # BUILD_ARG_KEY allows setting of arbitrary build args
    BUILD_ARG_NAME: value
```

The tagging strategy used in auto-tag:
- [if running on master branch] `latest`
- `<branchname|tagname>-latest` (example: `master-latest`, `0.0.1-latest`)
- `<branchname|tagname>-<short hash>` (example: `master-d3e0f6a2`, `0.0.1-d3e0f6a2`)
- [if running for a git tag] `tag-<tagname>` (example: `tag-1.2.3`)

These images can be automatically used in later CI jobs in the same pipeline using `image: $CI_REGISTRY_IMAGE/$NAME:$CI_COMMIT_REF_NAME-latest` or `image: $CI_REGISTRY_IMAGE/$NAME:$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA`.

### GitLab tag expiration policies
When using auto-tag, it is recommended to edit your project settings to have the following docker tag expiration policies.
- expire regex: .*
- preserve regex: ^(master|tag)-.+$|^latest$|^.*-latest$

## manual tagging
Example use without auto-tagging:
```yaml
jobname:
  stage: stage name
  extends: .auto_devops_docker_builder
  variables:
    CONTEXT_FOLDER: .gitlab/ci
    DOCKERFILE: builder.Dockerfile
    # [optional] name of the docker image (without registry.gitlab.cern.ch/namespace/projectname prefix)
    # if omitted, the resulting image will be registry.gitlab.cern.ch/namespace/projectname
    NAME: builder
    # BUILD_ARG_KEY allows setting of arbitrary build args
    BUILD_ARG_NAME: value
    # TAG[0-9]* specifies the list of tags for this image
    TAG: $CI_COMMIT_REF_NAME
```