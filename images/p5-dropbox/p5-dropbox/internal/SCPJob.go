package internal

import "os"

// SCPJob holds all info for an SSH client to perform a file copy
type SCPJob struct {
	LocalPath string
	Info      os.FileInfo
	File      *os.File
}

// SCPJobFrom given path create SCPJob
func SCPJobFrom(path string) (*SCPJob, error) {
	stat, err := os.Stat(path)
	if err != nil {
		return nil, err
	}
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	return &SCPJob{path, stat, file}, nil
}
