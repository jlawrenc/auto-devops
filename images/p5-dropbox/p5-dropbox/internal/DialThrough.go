package internal

import "golang.org/x/crypto/ssh"

func dialThrough(conn *ssh.Client, host string) (*ssh.Client, string, string, error) {
	subConn, err := conn.Dial("tcp", host+":22")
	if err != nil {
		return nil, "", "", err
	}
	conf, user, pass := GetSSHConfig(host)
	sshConn, cha, req, err := ssh.NewClientConn(subConn, host+":22", conf)
	if err != nil {
		return nil, "", "", err
	}
	return ssh.NewClient(sshConn, cha, req), user, pass, nil
}
