FROM cern/cc7-base:20200701-1.x86_64
LABEL maintainer="Cactus <cactus@cern.ch>"

RUN yum install -y make git rpm-build jq gcc gcc-c++ && \
    yum clean all

ENTRYPOINT ["/bin/bash", "-c"]
CMD ["/bin/bash"]