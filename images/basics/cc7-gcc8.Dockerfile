# this image builds on top of cc7.Dockerfile
ARG BASICS_CC7_IMAGE
FROM ${BASICS_CC7_IMAGE}
LABEL maintainer="Cactus <cactus@cern.ch>"

RUN yum install -y centos-release-scl devtoolset-8-gcc devtoolset-8-gcc-c++ rh-ruby27-ruby-devel && \
    scl enable rh-ruby27 -- gem install fpm && \
    yum clean all

ENTRYPOINT ["/usr/bin/scl", "enable", "devtoolset-8", "rh-ruby27", "--", "/bin/bash", "-c"]
CMD ["/bin/bash"]