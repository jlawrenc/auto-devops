package internal

import (
	"os"
)

// TemplateValues are values available to templates
type TemplateValues struct {
	UserData  map[string]string
	Generator generator
}

// GetTemplateValues returns a filled-in copy of TemplateValues
func GetTemplateValues(userData map[string]string) TemplateValues {
	return TemplateValues{userData, generator{getGitlabFlowImageName}}
}

type generator struct {
	GetGitlabFlowImageName func() string
}

// GetImageName returns an image name conform to the gitlab-flow preset in kaniko builder
func getGitlabFlowImageName() string {
	imageName := Env("CI_REGISTRY_IMAGE")
	if os.Getenv("DOCKER_IMAGE_NAME") != "" {
		imageName += "/" + os.Getenv("DOCKER_IMAGE_NAME")
	}
	tagName := os.Getenv("CI_COMMIT_TAG")
	if tagName != "" {
		return imageName + ":tag-" + tagName
	}
	return imageName + ":" + Env("CI_COMMIT_REF_NAME") + "-" + Env("CI_COMMIT_SHORT_SHA")
}
