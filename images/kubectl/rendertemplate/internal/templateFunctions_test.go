package internal

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

const envKey = "unsetenvkey"

type inputoutput struct {
	inputs    []string
	output    string
	maxLength int
}

func TestSlug(t *testing.T) {
	todo := []inputoutput{
		{[]string{"a"}, "a", 0},
		{[]string{"0123456789012345678901234567890123456789012345678901234567890123456789"}, "012345678901234567890123456789012345678901234567890123456789012", 0},
		{[]string{"aaa"}, "aa", 2},
		{[]string{"HeLlO"}, "hello", 0},
		{[]string{"a--a"}, "a-a", 0},
		{[]string{"-----a--a-----"}, "a-a", 0},
		{[]string{"a_--__-a"}, "a-a", 0},
		{[]string{"Hello, World!"}, "hello-world", 0},
		{[]string{"準備"}, "", 0},
		{[]string{"Hello, 準備!"}, "hello", 0},
		{[]string{"準備", "準備"}, "", 0},
		{[]string{"Hello, 準備!", "Hello, 準備!"}, "hello-hello", 0},
	}

	for _, expected := range todo {
		output := Slug(expected.maxLength, expected.inputs...)
		require.Equal(t, expected.output, output)
	}
}

func TestEnv(t *testing.T) {
	os.Unsetenv(envKey)
	require.Panics(t, func() { Env(envKey) })
	os.Setenv(envKey, "")
	require.Panics(t, func() { Env(envKey) })
	os.Setenv(envKey, "value")
	require.Equal(t, "value", Env(envKey))
	os.Unsetenv(envKey)
}

func TestEnvOr(t *testing.T) {
	os.Unsetenv(envKey)
	require.Equal(t, "default", EnvOr(envKey, "default"))
	os.Setenv(envKey, "")
	require.Equal(t, "otherdefault", EnvOr(envKey, "otherdefault"))
	os.Setenv(envKey, "value")
	require.Equal(t, "value", EnvOr(envKey, "default"))
	os.Unsetenv(envKey)
}

func TestIfEnv(t *testing.T) {
	os.Unsetenv(envKey)

	require.False(t, IfEnv(envKey))

	os.Setenv(envKey, "")
	require.False(t, IfEnv(envKey))

	os.Setenv(envKey, "val")
	require.True(t, IfEnv(envKey))

	os.Unsetenv(envKey)
}

func TestSlugEnv(t *testing.T) {
	os.Setenv(envKey, "---a--_--B---")
	output := SlugEnv(0, envKey)
	require.Equal(t, "a-b", output)
	os.Unsetenv(envKey)
}

func TestFuncMap(t *testing.T) {
	m := FuncMap()
	keys := make([]string, 0)
	for k := range m {
		keys = append(keys, k)
	}
	require.Len(t, m, 5)
	require.Contains(t, keys, "env")
	require.Contains(t, keys, "slug")
	require.Contains(t, keys, "slugEnv")
	require.Contains(t, keys, "ifEnv")
	require.Contains(t, keys, "envOr")
}
